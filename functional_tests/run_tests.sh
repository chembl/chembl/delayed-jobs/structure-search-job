#!/usr/bin/env bash

set -x

## ----------------------------------------------------------------------------------------------------------------------
## Simulate simple similarity job
## ----------------------------------------------------------------------------------------------------------------------
#
#rm -rf $PWD/functional_tests/output_1 || true
#mkdir -p $PWD/functional_tests/output_1
#PYTHONPATH=$PWD:$PYTHONPATH $PWD/structure_search_job/run_job.py $PWD/functional_tests/params/run_params_example.yml -n
#EXIT_CODE_1=$?
#
#if [[ $EXIT_CODE_1 -eq 0 ]]; then
#    echo "The job ran successfully, as expected"
#else
#    echo "The job failed! Please check"
#    exit 1
#fi
#
#ls -lah $PWD/functional_tests/output_1/results.json
#
## ----------------------------------------------------------------------------------------------------------------------
## Simulate simple similarity job using chembl id as search term
## ----------------------------------------------------------------------------------------------------------------------
#
#rm -rf $PWD/functional_tests/output_2 || true
#mkdir -p $PWD/functional_tests/output_2
#PYTHONPATH=$PWD:$PYTHONPATH $PWD/structure_search_job/run_job.py $PWD/functional_tests/params/run_params_example2.yml -n
#EXIT_CODE_1=$?
#
#if [[ $EXIT_CODE_1 -eq 0 ]]; then
#    echo "The job ran successfully, as expected"
#else
#    echo "The job failed! Please check"
#    exit 1
#fi
#
#ls -lah $PWD/functional_tests/output_2/results.json

# ----------------------------------------------------------------------------------------------------------------------
# Simulate simple substructure job
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/functional_tests/output_3 || true
mkdir -p $PWD/functional_tests/output_3
PYTHONPATH=$PWD:$PYTHONPATH $PWD/structure_search_job/run_job.py $PWD/functional_tests/params/run_params_example3.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_3/results.json

# ----------------------------------------------------------------------------------------------------------------------
# Simulate simple substructure job with chembl id as a search term
# ----------------------------------------------------------------------------------------------------------------------

rm -rf $PWD/functional_tests/output_4 || true
mkdir -p $PWD/functional_tests/output_4
PYTHONPATH=$PWD:$PYTHONPATH $PWD/structure_search_job/run_job.py $PWD/functional_tests/params/run_params_example4.yml -n
EXIT_CODE_1=$?

if [[ $EXIT_CODE_1 -eq 0 ]]; then
    echo "The job ran successfully, as expected"
else
    echo "The job failed! Please check"
    exit 1
fi

ls -lah $PWD/functional_tests/output_4/results.json

## ----------------------------------------------------------------------------------------------------------------------
## Simulate simple connectivity job
## ----------------------------------------------------------------------------------------------------------------------
#
#rm -rf $PWD/functional_tests/output_5 || true
#mkdir -p $PWD/functional_tests/output_5
#PYTHONPATH=$PWD:$PYTHONPATH $PWD/structure_search_job/run_job.py $PWD/functional_tests/params/run_params_example5.yml -n
#EXIT_CODE_1=$?
#
#if [[ $EXIT_CODE_1 -eq 0 ]]; then
#    echo "The job ran successfully, as expected"
#else
#    echo "The job failed! Please check"
#    exit 1
#fi
#
#ls -lah $PWD/functional_tests/output_5/results.json
