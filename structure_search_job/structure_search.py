"""
Module that does structure searches
"""
from urllib.parse import quote
import json
from enum import Enum
import os
import re

import requests


class StructureSearchError(Exception):
    """Base class for exceptions in this module."""


class SearchTypes(Enum):
    """
    Enumeration of the possible search types
    """
    SIMILARITY = 'SIMILARITY'
    SUBSTRUCTURE = 'SUBSTRUCTURE'
    CONNECTIVITY = 'CONNECTIVITY'

    def __str__(self):
        return self.value


def get_ws_base_url():
    """
    :return: the base url
    """
    ebi_base_url = os.environ.get('WS_BASE_URL')

    if ebi_base_url is None:
        raise StructureSearchError(f'You need to set the WS_BASE_URL env variable!')

    return ebi_base_url


def get_search_results(search_type, search_term, threshold=None, page_size=1000, server_connection=None):
    """
    Queries the web services and collects the results of the search, returns a dict with the results obtained
    :param search_type: type of the search (see SearchTypes)
    :param search_term: search term for te search
    :param threshold: threshold (if similarity search)
    :param page_size: the page size for getting the results from the ws
    :param server_connection: the object of the class ServerConnection used to report progress to the server
    """
    ws_base_url = get_ws_base_url()
    search_url = get_initial_search_url(ws_base_url, search_type, search_term, page_size, threshold)
    page_num = 1

    update_job_progress(0, f'Starting to load the results with the following initial url: {search_url}',
                        server_connection)

    results = {
        'search_results': []
    }

    results_loaded_until_now = 0
    more_results_to_load = True
    progress = 0
    while more_results_to_load:

        search_request = requests.get(search_url)
        print(f'loading page: {search_url}')
        print(f'result status: {search_request.status_code}')
        response = search_request.json()

        error_message = response.get('error_message')
        if error_message is not None:
            report_job_error(progress, error_message, server_connection)
            raise StructureSearchError(f'There was an error while loading the data from the web services: '
                                       f'{error_message}')

        current_results = response['molecules']
        for current_result in current_results:
            results['search_results'].append(current_result)
            results_loaded_until_now += 1

        total_results = response['page_meta']['total_count']

        if total_results == 0:
            # There are no results, it already finished
            progress = 100
        else:
            progress = int((results_loaded_until_now / total_results) * 100)

        update_job_progress(progress, f'Loaded page {page_num}: {search_url}', server_connection)
        print(f'Loaded page {page_num}: {search_url}')
        next_page = response['page_meta']['next']
        print(f'next_page: {next_page}')

        if next_page is not None:
            host = re.search(r'^https?://[^/]+', ws_base_url).group(0)
            search_url = f'{host}{next_page}'
            page_num += 1
        else:
            more_results_to_load = False

    return results


def get_initial_search_url(ws_base_url, search_type, search_term, page_size, threshold=None):
    """
    Returns the initial url for calling the web services and doing a structure search
    :param ws_base_url: base url of the web services
    :param search_type: type of search, see Search Types
    :param search_term: a smiles or chembl id to make the search against
    :param page_size: amount of items to get each call
    :param threshold: if similarity, the threshold for the search
    """
    encoded_search_term = quote(search_term)
    if search_type == str(SearchTypes.SIMILARITY):
        return f'{ws_base_url}/similarity/{encoded_search_term}/{threshold}.json?limit={page_size}' \
               f'&only=molecule_chembl_id,similarity'

    if search_type == str(SearchTypes.SUBSTRUCTURE):
        return f'{ws_base_url}/substructure/{encoded_search_term}.json?limit={page_size}' \
               f'&only=molecule_chembl_id'
    if search_type == str(SearchTypes.CONNECTIVITY):
        return f'{ws_base_url}/molecule.json?limit={page_size}&only=molecule_chembl_id' \
               f'&molecule_structures__canonical_smiles__flexmatch={encoded_search_term}'

    raise StructureSearchError(f'The search type {search_type} does not exist!')


def save_output_file(output_file_path, search_results, server_connection):
    """
    saves the search results in the path indicated as parameter
    :param output_file_path: path of the output file
    :param search_results: results of the search
    :param server_connection: the object of the class ServerConnection used to report progress to the server
    """
    with open(output_file_path, 'wt') as output_file:
        output_file.write(json.dumps(search_results))

    update_job_progress(100, 'Output file written!', server_connection)


def update_job_progress(progress, status_msg, server_connection=None):
    """
    reports the progress and sends the message to the status log. or prints to the console if there is no
    server_connection (is None)
    :param progress: progress percentage
    :param status_msg: message to send to the log
    :param server_connection: the object of the class ServerConnection used to report progress to the server
    """
    if server_connection is None:
        print(f'progress: {progress} log: {status_msg}')
    else:
        server_connection.update_job_progress(progress, status_log_msg=status_msg)


def report_job_error(progress, error_msg, server_connection=None):
    """
    reports to the server the
    :param progress: progress percentage
    :param error_msg: error message to send
    :param server_connection: the object of the class ServerConnection used to report progress to the server
    """
    if server_connection is None:
        print(f'ERROR! progress was: {progress} msg: {error_msg}')
    else:
        status_description = json.dumps({'error_msg': error_msg})
        server_connection.update_job_progress(progress, status_description=status_description)
