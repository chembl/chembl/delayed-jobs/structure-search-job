#!/usr/bin/env python3
# pylint: disable=too-many-locals, too-few-public-methods
"""
Script that runs a job that does structure searches
"""
import argparse
import os
from pathlib import Path
import time

import yaml

from structure_search_job import structure_search
from structure_search_job.server_connection import DelayedJobsServerConnection

PARSER = argparse.ArgumentParser()
PARSER.add_argument('run_params_file', help='The path of the file with the run params')
PARSER.add_argument('-v', '--verbose', help='Make output verbose', action='store_true')
PARSER.add_argument('-n', '--dry-run', help='Do not actually send progress to the delayed jobs server',
                    action='store_true')
ARGS = PARSER.parse_args()


def run():
    """
    Runs the job
    """

    initial_time = time.time()

    run_params = yaml.load(open(ARGS.run_params_file, 'r'), Loader=yaml.FullLoader)
    job_params = run_params.get('job_params')

    search_type = job_params.get('search_type').upper()
    threshold = job_params.get('threshold')
    search_term = job_params.get('search_term')

    server_conn = DelayedJobsServerConnection(run_params_file_path=ARGS.run_params_file, dry_run=ARGS.dry_run)

    custom_config = run_params.get('custom_job_config')
    print('custom_config: ', custom_config)
    if custom_config is not None:
        load_custom_config_as_env_variables(custom_config)

    search_results = structure_search.get_search_results(search_type, search_term, threshold, page_size=1000,
                                                         server_connection=server_conn)

    output_path = run_params.get('output_dir')
    output_file_path = Path.joinpath(Path(output_path), 'results.json')

    structure_search.save_output_file(output_file_path, search_results, server_conn)

    end_time = time.time()
    time_taken = end_time - initial_time
    stats_dict = {
        'search_type': search_type,
        'time_taken': time_taken
    }
    server_conn.send_job_statistics(stats_dict)


def load_custom_config_as_env_variables(custom_config):
    """
    Loads the custom configuration dict into the env variables
    :param custom_config: the custom configuration for the job
    """
    for key, value in custom_config.items():
        os.environ[key.upper()] = str(value)


if __name__ == "__main__":
    run()
