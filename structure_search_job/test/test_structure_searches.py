"""
This Module tests the basic functions job
"""
import unittest
from urllib.parse import quote

from structure_search_job import structure_search

class TestStructureSearches(unittest.TestCase):
    """
    Class to test the functions used in the structure searches
    """

    def test_generates_request_params_similarity(self):
        """
        Tests that generates the initial url correctly for similarity
        """
        ws_base_url = 'https://www.ebi.ac.uk/chembl/api/data'
        search_type = str(structure_search.SearchTypes.SIMILARITY)
        search_term = 'CN1C(=O)C=C(c2cccc(Cl)c2)c3cc(ccc13)[C@@](N)(c4ccc(Cl)cc4)c5cncn5C'
        threshold = 80
        page_size = 10000

        url_must_be = f'{ws_base_url}/similarity/{quote(search_term)}/{threshold}.json?limit={page_size}' \
                      f'&only=molecule_chembl_id,similarity'

        url_got = structure_search.get_initial_search_url(ws_base_url, search_type, search_term, page_size, threshold)
        self.assertEqual(url_must_be, url_got, msg='The initial url was not generated correctly for similarity!')


    def test_generates_initial_url_substructure(self):
        """
        Tests that generates the initial url correctly for substructure
        """
        ws_base_url = 'https://www.ebi.ac.uk/chembl/api/data'
        search_type = str(structure_search.SearchTypes.SUBSTRUCTURE)
        search_term = 'CN1C(=O)C=C(c2cccc(Cl)c2)c3cc(ccc13)[C@@](N)(c4ccc(Cl)cc4)c5cncn5C'
        page_size = 10000

        url_must_be = f'{ws_base_url}/substructure/{quote(search_term)}.json?limit={page_size}' \
                      f'&only=molecule_chembl_id'

        url_got = structure_search.get_initial_search_url(ws_base_url, search_type, search_term, page_size)
        self.assertEqual(url_must_be, url_got, msg='The initial url was not generated correctly for substructure!')

    def test_generates_initial_url_connectivity(self):
        """
        Tests that generates the initial url correctly for connectivity
        """
        ws_base_url = 'https://www.ebi.ac.uk/chembl/api/data'
        search_type = str(structure_search.SearchTypes.CONNECTIVITY)
        search_term = 'CN1C(=O)C=C(c2cccc(Cl)c2)c3cc(ccc13)[C@@](N)(c4ccc(Cl)cc4)c5cncn5C'
        page_size = 10000

        url_must_be = f'{ws_base_url}/molecule.json?limit={page_size}&only=molecule_chembl_id' \
                      f'&molecule_structures__canonical_smiles__flexmatch={quote(search_term)}'

        url_got = structure_search.get_initial_search_url(ws_base_url, search_type, search_term, page_size)
        self.assertEqual(url_must_be, url_got, msg='The initial url was not generated correctly for connectivity!')


    def test_fails_when_search_type_not_valid(self):
        """
        Tests that fails when an invalid search type is provided
        """

        ws_base_url = 'https://www.ebi.ac.uk/chembl/api/data'
        search_type = 'nonexistent_type'
        search_term = 'CN1C(=O)C=C(c2cccc(Cl)c2)c3cc(ccc13)[C@@](N)(c4ccc(Cl)cc4)c5cncn5C'
        page_size = 10000

        with self.assertRaises(structure_search.StructureSearchError,
                               msg='An exception should have been raised! The search type does not exist'):
            structure_search.get_initial_search_url(ws_base_url, search_type, search_term, page_size)


    def test_gets_similarity_results_from_smiles(self):
        """
        Tests that it gets the results of a search from a smiles
        """

        search_type = str(structure_search.SearchTypes.SIMILARITY)
        search_term = 'CC(=O)Oc1ccccc1C(=O)O'
        threshold = 40
        page_size = 100 # so it is forced to iterate over the pages

        results_got = structure_search.get_search_results(search_type, search_term, threshold, page_size)
        self.assertIsNotNone(results_got, msg='The results were not obtained!')

        search_results_got = results_got.get('search_results')
        self.assertIsNotNone(search_results_got, msg='There are no results in the final object!')
